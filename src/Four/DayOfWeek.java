package Four;

public enum DayOfWeek {

    Saturday("شنبه", false), Sunday("یک شنبه", false), Monday("دو شنبه", false),
    Tuesday("سه شنبه", false), Wednesday("چهار شنبه", false), Thursday("پنج شنبه", false),
    Friday("جمعه", true);

    private String persianName;
    private boolean holiday;

    DayOfWeek(String persianName, boolean holiday) {
        this.persianName = persianName;
        this.holiday = holiday;
    }

    public static void main(String[] args) {

        System.out.println(DayOfWeek.Friday);

    }

    @Override
    public String toString() {
        if (holiday) {
            return " تعطیل " + persianName;
        } else {
            return persianName;
        }
    }

}

