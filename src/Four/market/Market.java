package Four.market;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Market {

    private final java.util.Random rand = new java.util.Random();
    // consider using a Map<String,Boolean> to say whether the identifier is being used or not
    private final Set<String> identifiers = new HashSet<>();


    private Shelf[] shelves;
    private int shelvesCount;
    private int count = 0;

    public Market(int shelvesCount) {
        this.shelvesCount = shelvesCount;
        shelves = new Shelf[shelvesCount];
    }

    public static void main(String[] args) {

        Market market = new Market(20);
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            Shelf shelf = new Shelf(40);
            for (int j = 0; j < 20; j++) {
                shelf.addProduct(new Product(market.randomIdentifier(),
                        market.randomIdentifier(), random.nextInt(1000)));
            }
            market.addShelf(shelf);
        }

        market.shelves[2].sortByBrand();
        System.out.println();
        market.shelves[2].sortByCost();
        System.out.println();
        market.shelves[2].sortByName();

    }

    public void addShelf(Shelf shelf) {
        if (count < shelvesCount) {
            shelves[count] = shelf;
            count++;
        }
    }

    public void findProduct(String name) {
        for (int i = 0; i < count; i++) {
            shelves[i].findProduct(name);
        }
    }

    private String randomIdentifier() {
        StringBuilder builder = new StringBuilder();
        while (builder.toString().length() == 0) {
            int length = rand.nextInt(5) + 5;
            for (int i = 0; i < length; i++) {
                //in 3 ta vase test an
                String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            }
            if (identifiers.contains(builder.toString())) {
                builder = new StringBuilder();
            }
        }
        return builder.toString();
    }

}
