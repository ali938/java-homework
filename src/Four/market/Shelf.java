package Four.market;

import java.util.ArrayList;

public class Shelf {

    private static int maxProduct = 50;
    private int productCount;
    private ArrayList<Product> products;
    private int count;

    public Shelf(int productCount) {
        if (productCount > maxProduct) {
            this.productCount = maxProduct;
        } else {
            this.productCount = productCount;
        }
        products = new ArrayList<>();
        count = 0;
    }

    public void addProduct(Product product) {
        if (count < productCount) {
            products.add(product);
            count++;
        }
    }

    public void addMultipleProducts(int number, String product) {
        String[] productStrings = product.split("\\*");
        for (int i = 0; i < number; i++) {
            String p = productStrings[i];
            String[] temp = p.split(",");
            products.add(new Product(temp[0], temp[1], Integer.parseInt(temp[2])));
            count++;
        }
    }

    private void removeByBrand(String brand) {
        ArrayList<Product> temp = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (products.get(i).getBrand().equals(brand)) {
                temp.add(products.get(i));
            }
        }
        count -= temp.size();
        products.removeAll(temp);
    }

    public void removeByName(String name) {
        ArrayList<Product> temp = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            if (products.get(i).getName().equals(name)) {
                temp.add(products.get(i));
            }
        }
        count -= temp.size();
        products.removeAll(temp);
    }

    public void sortByBrand() {
        ArrayList<Product> out = new ArrayList<>();
        int j;
        for (int i = 0; i < count; i++) {
            Product temp = products.get(i);
            j = 0;
            for (; j < out.size(); j++) {
                if (temp.getBrand().compareTo(out.get(j).getBrand()) <= 0) {
                    break;
                }
            }
            out.add(j, temp);
        }
        show(out);
    }

    public void sortByCost() {
        ArrayList<Product> out = new ArrayList<>();
        int j;
        for (int i = 0; i < count; i++) {
            Product temp = products.get(i);
            j = 0;
            for (; j < out.size(); j++) {
                if (temp.getPrice() > out.get(j).getPrice()) {
                    break;
                }
            }
            out.add(j, temp);
        }
        show(out);
    }

    public void sortByName() {
        ArrayList<Product> out = new ArrayList<>();
        int j;
        for (int i = 0; i < count; i++) {
            Product temp = products.get(i);
            j = 0;
            for (; j < out.size(); j++) {
                if (temp.getName().compareTo(out.get(j).getName()) <= 0) {
                    break;
                }
            }
            out.add(j, temp);
        }
        show(out);
    }

    private void show(ArrayList<Product> products) {
        for (Product p : products) {
            p.print();
        }
    }

    public void show() {
        for (Product p : products) {
            p.print();
        }
    }

    public void findProduct(String name) {
        for (int i = 0; i < count; i++) {
            if (products.get(i).getName().equals(name)) {
                products.get(i).print();
            }
        }
    }

}
