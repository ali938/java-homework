package One;

import java.util.Scanner;

public class MaxSum {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int numberCount = scanner.nextInt();
        scanner.nextLine();
        int[] numbers = new int[numberCount];
        for (int i = 0; i < numberCount; i++) {
            numbers[i] = scanner.nextInt();
        }
        scanner.nextLine();

        int maxSum = 0;
        int maxFirst = -1;
        int maxLast = -1;
        int sum = 0;
        int first = 0;
        int last = 0;
        for (int i = 0; i < numberCount; i++) {
            sum += numbers[i];
            last++;
            if (sum < 0) {
                sum = 0;
                first = i + 1;
                last = i + 1;
            } else if (sum > maxSum) {
                maxFirst = first;
                maxLast = last;
                maxSum = sum;
            }
        }

        if (maxFirst != -1) {
            for (int i = maxFirst; i < maxLast; i++) {
                System.out.print(numbers[i]);
                System.out.print(" ");
            }
            System.out.println();
            System.out.println("sum = " + maxSum);
        } else {
            System.out.println("all numbers are negative");
        }

    }

}
