package One;

import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.nextLine();

        int digitCount = (int) Math.ceil(Math.log10(number));
        int[] digit = new int[digitCount];
        for (int i = 0; i < digitCount; i++) {
            digit[i] = number % 10;
            number = (number - digit[i]) / 10;
        }

        int j = digitCount - 1;
        for (int i = 0; i < digitCount / 2; i++, j--) {
            if (digit[i] != digit[j]) {
                System.out.println("NO");
                return;
            }

        }
        System.out.println("YES");
    }

}
