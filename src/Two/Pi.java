package Two;

import java.util.Random;
import java.util.Scanner;

public class Pi {

    public static void main(String[] args) {

        double pi;

        int r = 100;
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();


        double hit = 0;
        int x, y;
        double distance;
        for (int i = 0; i < count; i++) {
            x = random.nextInt(r);
            y = random.nextInt(r);

            distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
            if (distance < r) {
                hit++;
            }
        }

        pi = 4 * (hit / count);
        System.out.println(pi);

    }

}
